#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 15:21:07 2019

@author: sebastian
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as seabornInstance
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
#%matplotlib inline

dataset = pd.read_csv('dataset', sep=" ", header=None)
dataset.columns = ['Time', 'State', 'Temperature', 'Relay']
dataset.shape
dataset.describe()

dataset.plot(x='Time', y='Temperature', style='o')
plt.title('Time of Day vs Temperature')
plt.xlabel('Time of Day')
plt.ylabel('Temperature')
plt.show()

seabornInstance.distplot(dataset['Temperature'])
plt.show()

x = dataset['Time'].values.reshape(-1, 1)
y = dataset['Temperature'].values.reshape(-1, 1)
x_train, x_test, y_train, y_test = train_test_split(x, y,
                                                    test_size=0.2,
                                                    random_state=0)

regressor = LinearRegression()
regressor.fit(x_train, y_train)  # training the algorithm

print('Intercept:', regressor.intercept_)  # retrieve the intercept
print('Slope:', regressor.coef_)  # retrieve the slope

y_pred = regressor.predict(x_test)
df = pd.DataFrame({'Actual': y_test.flatten(), 'Predicted': y_pred.flatten()})
df
df1 = df.head(24)
df1.plot(kind='bar', figsize=(16, 10))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()

plt.scatter(x_test, y_test,  color='gray')
plt.plot(x_test, y_pred, color='red', linewidth=2)
plt.show()

print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error
                                          (y_test, y_pred)))
