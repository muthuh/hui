from binascii import hexlify
import functools
from flask import Blueprint, flash, g, redirect, render_template, request,\
                    session, url_for
from hui.db import get_db
from os import urandom
from werkzeug.security import check_password_hash, generate_password_hash

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/', methods=('GET', 'POST'))
def index():

    return render_template('cp.index')


@bp.route('/register', methods=('GET', 'POST'))
def register():

    if request.method == 'POST':

        user = request.form['user']
        passphrase = request.form['passphrase']
        db = get_db()

        error = None
        if not user or not passphrase:

            error = ''

        elif db.execute('SELECT user FROM users WHERE user = ?',
                        (user,)).fetchone() is not None:

            error = 'Cannot register \'{}\'.'.format(user)

        if error is None:

            key_list = db.execute('SELECT key FROM users')
            key = hexlify(urandom(16)).decode()
            while key in key_list:
                key = hexlify(urandom(16)).decode()
            db.execute('INSERT INTO users (user, passphrase, key)'
                       'VALUES (?, ?, ?)',
                       (user, generate_password_hash(passphrase), key))
            db.commit()
            session.clear()
            session['user'] = user
            return redirect(url_for('cp.index'))

        flash(error)

    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():

    if request.method == 'POST':

        user = request.form['user']
        passphrase = request.form['passphrase']
        db = get_db()
        user = db.execute('SELECT * FROM users WHERE user = ?',
                          (user,)).fetchone()

        error = None
        if user is None or not check_password_hash(user['passphrase'],
                                                   passphrase):
            error = ''

        elif error is None:

            session.clear()
            session['user'] = user['user']
            return redirect(url_for('cp.index'))

        flash(error)

    return render_template('auth/login.html')


@bp.before_app_request
def load_logged_in_user():

    user = session.get('user')

    if user is None:

        g.user = None

    else:

        g.user = get_db().execute('SELECT * FROM users WHERE user = ?',
                                  (user,)).fetchone()


@bp.route('/logout')
def logout():

    session.clear()
    return redirect(url_for('cp.index'))


def login_required(view):

    @functools.wraps(view)
    def wrapped_view(**kwargs):

        if g.user is None:

            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
