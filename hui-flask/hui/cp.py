from flask import Blueprint, flash, g, redirect, render_template,\
                     request, url_for
from werkzeug.exceptions import abort
from hui.auth import login_required
from hui.db import get_db

bp = Blueprint('cp', __name__)


@bp.route('/cp')
@login_required
def index():
    db = get_db()
    user_info = db.execute(
        'SELECT * FROM users WHERE user = ?', (g.user['user'],)).fetchone()
    return render_template('cp/index.html', user_info=user_info)
