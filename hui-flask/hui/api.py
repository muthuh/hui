from flask import Blueprint, request, render_template
from hui.db import get_db

bp = Blueprint('api', __name__)


@bp.route('/api', methods=('GET', 'PUT'))
def index():

    db = get_db()

    if request.method == 'GET':

        db = get_db()
        key = request.args.get('key')
        query_result = db.execute('SELECT ip FROM users WHERE key = ?',
                                  (key,)).fetchone()

        return render_template('api/index.html', query_result=query_result)

    if request.method == 'PUT':

        ip = request.form['ip']
        key = request.form['key']

        db.execute('UPDATE users SET ip = ? WHERE key = ?',
                   (ip, key)).fetchone()
        db.commit()

        return ''
