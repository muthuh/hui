import ds18x20
import network
from machine import ADC, reset, Pin, Timer, unique_id
from onewire import OneWire
import socket
from time import sleep_ms
from ubinascii import hexlify

SSID = 'CableComm-61624c'  # adjustable
PASSPHRASE = '225522662'

MSG_SIZE = 256  # internals
PERIOD_MS_MAKE_DO = 60000
PERIOD_MS_MAKE_DO_ONESHOT = 12000
PIN_BUTTON = 0
PIN_LED = 2
PIN_RELAY = 5
PIN_RGB_R = 14
PIN_RGB_G = 12
PIN_RGB_B = 13
PIN_SENSOR = 4
PORT_NO = 48414


class Thermostat():

    def __init__(self):

        self.button = Pin(PIN_BUTTON, Pin.IN, Pin.PULL_UP)
        self.id = hexlify(unique_id()).decode()
        self.led = Pin(PIN_LED, Pin.OUT)
        self.pot = ADC(0)
        self.relay = Pin(PIN_RELAY, Pin.OUT)
        pin_onewire = OneWire(Pin(PIN_SENSOR))
        self.sens = ds18x20.DS18X20(pin_onewire)
        self.sensors = self.sens.scan()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', PORT_NO))
        self.state = 'False'

    def hard_reset(self, a_pin):

        reset()

    def measure_temp(self):

        self.sens.convert_temp()
        sleep_ms(750)
        for a_sensor in self.sensors:
            self.sens.temp = self.sens.read_temp(a_sensor)

    def make_do(self):

        self.led.value(0)

        try:

            if self.sync_counter < 1:

                del self.sync_counter
                del self.parent
                self.relay.value(0)
                self.sync_counter == 0  # immediate SYN

            else:

                self.measure_temp()
                self.relay_adjust()
                self.pot_val = self.pot.read()
                self.sync_counter -= 1
                self.sock.sendto('{} {} {} {}'.format(self.id,
                                 self.relay.value(), self.sens.temp,
                                 self.pot_val), self.parent)
                self.led.value(1)

        except AttributeError:

            self.relay.value(0)
            self.sock.sendto('SYN', ('255.255.255.255', PORT_NO))
            print('SYN')

    def relay_adjust(self):

        if self.state == 'True':

            self.relay.value(1)

        else:

            self.relay.value(0)


ap = network.WLAN(network.AP_IF)  # deactivate ap, connect wlan
ap.active(False)
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
if not wlan.isconnected():
    wlan.connect(SSID, PASSPHRASE)
    while not wlan.isconnected():
        pass

this_dev = Thermostat()

this_dev.button.irq(trigger=Pin.IRQ_FALLING, handler=this_dev.hard_reset)

vt_make_do_oneshot = Timer(-1)
vt_make_do_oneshot.init(period=PERIOD_MS_MAKE_DO_ONESHOT, mode=Timer.ONE_SHOT,
                        callback=lambda t: this_dev.make_do())
vt_make_do = Timer(-1)
vt_make_do.init(period=PERIOD_MS_MAKE_DO, mode=Timer.PERIODIC,
                callback=lambda t: this_dev.make_do())

while True:

    this_dev.sock_inc = this_dev.sock.recvfrom(MSG_SIZE)
    data_content = this_dev.sock_inc[0].decode().split()
    this_dev.led.value(0)

    try:

        if this_dev.parent == this_dev.sock_inc[1]:

            this_dev.sync_counter = 1
            print('PAR \'{}\''.format(data_content))
            this_dev.state = data_content[0]
            this_dev.relay_adjust()
            this_dev.led.value(1)

    except AttributeError:

        if data_content[0] == 'SYN-ACK':

            print('PAR \'{}\''.format(data_content))
            this_dev.parent = this_dev.sock_inc[1]
            this_dev.state = data_content[1]
            this_dev.relay_adjust()
            this_dev.sync_counter = 1
            this_dev.led.value(1)
