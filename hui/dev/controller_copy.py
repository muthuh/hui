def controller(a_logger, port_no=48414):

    from datetime import datetime
    from rc import location, country_code, owm_api_key, data_prefix,\
        provider, provider_api_key
    from pathlib import Path
    from requests import get, put
    from twisted.internet import task
    from twisted.internet import reactor
    from twisted.internet.protocol import DatagramProtocol

    Path('{}/reports/outside'.format
         (data_prefix)).mkdir(parents=True, exist_ok=True)

    class Operator(DatagramProtocol):

        def __init__(self):

            # self.info_loop = task.LoopingCall(self.display_info)
            self.nodes = set()
            self.state = False
            self.auto = False
            self.threshrate = 0.12  # must <1
            self.threshspan = 1.618
            self.threshold = 16.382
            self.threshnew = self.threshold

        def startProtocol(self):

            a_logger.info('Threshold {}°C'.format(self.threshold))
            # self.info_loop.start(360, False)

        def display_info(self):

            try:
                a_logger.info('Average {0:.2f}°C'.format(self.threshavg))
            except AttributeError:
                pass
            a_logger.info('Threshold {}°C'.format(self.threshold))
            a_logger.info('State \'{}\''.format(self.state))

        def state_adjust(self):

            if self.state\
                    and self.threshold-self.threshspan != self.threshnew:

                if self.threshnew < self.threshold-self.threshspan:

                    a_logger.debug('State \'{}\' --> \'False\''.format
                                   (self.state))
                    self.state = False
                    a_logger.debug('Threshold {} --> {}°C'.format
                                   (self.threshold, self.threshnew))
                    self.threshold = self.threshnew

                else:

                    a_logger.debug('Threshold {} --> {}°C'.format
                                   (self.threshold,
                                    self.threshnew+self.threshspan))
                    self.threshold = self.threshnew+self.threshspan

            elif not self.state and self.threshold != self.threshnew:

                a_logger.debug('Threshold {} --> {}°C'.format
                               (self.threshold, self.threshnew))
                self.threshold = self.threshnew

            if self.state and self.threshavg > self.threshold:

                a_logger.info('State \'{}\' --> \'False\''.format
                              (self.state))
                self.state = False
                self.threshold = self.threshold-self.threshspan
                a_logger.info('Average {0:.2f}°C'.format(self.threshavg))
                a_logger.info('Threshold -{} ({}°C)'.
                              format(self.threshspan, self.threshold))

            elif not self.state and self.threshavg < self.threshold:

                a_logger.info('State \'{}\' --> \'True\''.format
                              (self.state))
                self.state = True
                self.threshold = self.threshold+self.threshspan
                a_logger.info('Average {0:.2f}°C'.format(self.threshavg))
                a_logger.info('Threshold +{} ({}°C)'.
                              format(self.threshspan, self.threshold))

        def auto_adjust(self):

            pass

        def write_report(self, content):

            today = datetime.now().strftime('%Y-%m-%d')
            time = datetime.now().strftime('%H:%M:%S')

            try:

                with open('{}/reports/{}/{}'.format
                          (data_prefix, content[0], today),
                          'a') as report_file:
                    report_file.write('{} {} {} {}\n'.format(time, content[1],
                                      content[2], content[3]))

            except FileNotFoundError:

                Path('{}/reports/{}'.format(data_prefix, content[0])).mkdir(
                        parents=True, exist_ok=True)
                with open('{}/reports/{}/{}'.format
                          (data_prefix, content[0], today),
                          'a') as report_file:
                    report_file.write('{} {} {} {}\n'.format(time, content[1],
                                      content[2], content[3]))

        def datagramReceived(self, data, addr):

            try:

                data_content = data.decode().split()

                if data_content[0] == 'T' and float(data_content[1]):

                    data_content[1] = float(data_content[1])

                    if data_content[1] == -1 and self.auto:

                        a_logger.debug('\'{}\' \'{}\''.format
                                       (addr, data_content))
                        a_logger.info('Auto \'{}\' --> \'False\''.format
                                      (self.auto))
                        self.auto = False

                    if data_content[1] == 1 and not self.auto:

                        a_logger.debug('\'{}\' \'{}\''.format
                                       (addr, data_content))
                        a_logger.info('Auto \'{}\' --> \'True\''.format
                                      (self.auto))
                        self.auto = True

                    elif data_content[1] > 1:

                        a_logger.debug('\'{}\' \'{}\''.format
                                       (addr, data_content))
                        self.threshnew = float(data_content[1])
                        a_logger.info('Threshold {}°C'.format(self.threshnew))

                elif addr in self.nodes:

                    a_logger.debug('\'{}\' \'{}\''.format(addr, data_content))

                    try:

                        self.threshavg = (((1-self.threshrate) *
                                           self.threshavg) +
                                          (self.threshrate * float
                                           (data_content[2])))

                    except AttributeError:

                        self.threshavg = float(data_content[2])
                        a_logger.info('Initial average {}°C'.format
                                      (self.threshavg))

                    if self.auto:

                        self.auto_adjust()

                    else:

                        self.state_adjust()

                    a_logger.debug('Average {0:.2f}°C'.format(self.threshavg))
                    a_logger.debug('Threshold {}°C'.format(self.threshold))
                    self.write_report(data_content)
                    self.transport.write('{}'.format(self.state).encode(),
                                         addr)
                    a_logger.debug('\'{}\' --> \'{}\''.format
                                   (self.state, addr))

                elif addr[1] == port_no and data == b'SYN':

                    a_logger.debug('\'{}\' \'{}\''.format(addr, data_content))
                    self.nodes.add(addr)
                    self.transport.write('SYN-ACK {}'.format
                                         (self.state).encode(), addr)
                    a_logger.debug('\'SYN-ACK {}\' --> \'{}\''.format
                                   (self.state, addr))
                    a_logger.info('\'{}\' paired'.format(addr[0]))

                else:

                    a_logger.warn('\'{}\' \'{}\''.format(addr, data_content))

            except (IndexError, ValueError):

                a_logger.warn('\'{}\' \'{}\''.format(addr, data_content))

                if data == b'SYN' and addr[1] == port_no:

                    self.transport.write('SYN-ACK {}'.format
                                         (self.state).encode(), addr)
                    a_logger.debug('\'SYN-ACK {}\' --> \'{}\''.format
                                   (self.state, addr))
                    a_logger.info('\'{}\' paired'.format(addr[0]))

    def weather_parse():

        today = datetime.now().strftime('%Y-%m-%d')
        time = datetime.now().strftime('%H:%M:%S')

        resp = get("http://api.openweathermap.org/data/2.5/"
                   "weather?appid={}&q={},{}&units=metric"
                   .format(owm_api_key, location, country_code))

        with open('{}/reports/outside/{}'.format
                  (data_prefix, today), 'a') as report_file:
            report_file.write('{} {}\n'.format(time, resp.json().get
                              ('main').get('temp')))

        a_logger.debug('{},{} {}°C'.format(location, country_code,
                       resp.json().get('main').get('temp')))

    def provider_link():

        resp = get("http://ip.42.pl/raw")
        print(resp)
        payload = {'ip': resp, 'key': provider_api_key}
        put(provider, data=payload)

    reactor.listenUDP(port_no, Operator())
    a_logger.info('Listening on {}'.format(port_no))

#    provider_loop = task.LoopingCall(provider_link)
#    provider_loop.start(3600, True)

    weather_loop = task.LoopingCall(weather_parse)
    weather_loop.start(360, True)

    reactor.run()
