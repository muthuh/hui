import ds18x20
import network
from machine import ADC, reset, Pin, Timer, unique_id
from onewire import OneWire
import socket
from time import sleep_ms
from ubinascii import hexlify

SSID = 'CableComm-61624c'  # adjustable
PASSPHRASE = '225522662'

MSG_SIZE = 1024  # internals
PERIOD_MS = 60000
PIN_BUTTON = 0
PIN_SENSOR = 4
PIN_RELAY = 5
PIN_RGB_R = 14
PIN_RGB_G = 12
PIN_RGB_B = 13
PORT_NO = 48413


class Alarm():

    def __init__(self):

        self.button = Pin(PIN_BUTTON, Pin.IN, Pin.PULL_UP)
        self.id = hexlify(unique_id()).decode()
        self.pot = ADC(0)
        self.relay = Pin(PIN_RELAY, Pin.OUT)
        pin_onewire = OneWire(Pin(PIN_SENSOR))
        self.rgb_r = Pin(PIN_RGB_R, Pin.OUT)
        self.rgb_g = Pin(PIN_RGB_G, Pin.OUT)
        self.rgb_g.value(1)
        self.rgb_b = Pin(PIN_RGB_B, Pin.OUT)
        self.rgb_b.value(1)
        self.sens = ds18x20.DS18X20(pin_onewire)
        self.sensors = self.sens.scan()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', PORT_NO))

    def hard_reset(self, a_pin):

        reset()

    def measure_temp(self):

        self.sens.convert_temp()
        sleep_ms(750)
        for a_sensor in self.sensors:
            self.sens.temp = self.sens.read_temp(a_sensor)

    def make_do(self):

        self.rgb_b.value(0)

        try:

            if self.sync_counter <= 0:

                del self.sync_counter
                del self.parent

            else:

                self.measure_temp()
                self.pot_val = self.pot.read()
                self.sock.sendto('{} {} {} {}'.format(self.id,
                                 self.relay.value(), self.sens.temp,
                                 self.pot_val), self.parent)
                self.sync_counter -= 1

        except AttributeError:

            self.sock.sendto('SYN', ('255.255.255.255', PORT_NO))
            print('SYN')
            self.rgb_r.value(0)
            self.rgb_g.value(1)

        self.rgb_b.value(1)


ap = network.WLAN(network.AP_IF)  # deactivate ap, connect wlan
ap.active(False)
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
if not wlan.isconnected():
    wlan.connect(SSID, PASSPHRASE)
    while not wlan.isconnected():
        pass

this_dev = Alarm()

this_dev.button.irq(trigger=Pin.IRQ_FALLING, handler=this_dev.hard_reset)

vt_make_do_oneshot = Timer(-1)
vt_make_do_oneshot.init(period=4000, mode=Timer.ONE_SHOT,
                        callback=lambda t: this_dev.make_do())
'''vt_make_do_oneshot_again = Timer(-1)
vt_make_do_oneshot_again.init(period=20000, mode=Timer.ONE_SHOT,
                              callback=lambda t: this_dev.make_do())'''
vt_make_do = Timer(-1)
vt_make_do.init(period=PERIOD_MS, mode=Timer.PERIODIC,
                callback=lambda t: this_dev.make_do())


while True:

    this_dev.sock_inc = this_dev.sock.recvfrom(MSG_SIZE)
    this_dev.rgb_b.value(0)

    try:

        if this_dev.parent == this_dev.sock_inc[1]:

            print('PAR \'{}\''.format(this_dev.sock_inc[0].decode()))

            if this_dev.sock_inc[0].decode() == 'ON':
                this_dev.relay.value(1)

            if this_dev.sock_inc[0].decode() == 'OFF':
                this_dev.relay.value(0)

            this_dev.sync_counter += 1

    except AttributeError:

        if this_dev.sock_inc[0].decode() == 'SYN-ACK':

            this_dev.parent = this_dev.sock_inc[1]
            this_dev.sock.sendto('ACK', this_dev.parent)
            print('ACK')
            this_dev.sync_counter = 2
            this_dev.rgb_r.value(1)
            this_dev.rgb_g.value(0)

    this_dev.rgb_b.value(1)
