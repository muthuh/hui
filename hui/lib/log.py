def define_logger(a_logfile, debug_mode):

    import logging

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    file_log = logging.FileHandler(a_logfile)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    file_log.setFormatter(formatter)
    file_log.setLevel(logging.DEBUG)

    stream_log = logging.StreamHandler()
    formatter = logging.Formatter('%(levelname)s %(message)s')
    stream_log.setFormatter(formatter)
    if debug_mode:
        stream_log.setLevel(logging.DEBUG)
    else:
        stream_log.setLevel(logging.INFO)

    logger.addHandler(file_log)
    logger.addHandler(stream_log)

    return logger
