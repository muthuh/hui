def print_surround(a_string, input_bool=False, top_surr=True, bot_surr=True):

    def surround(anInt, a_char=''):
        for _ in range(anInt):
            print(a_char, end='')
        print('')

    if input_bool:
        print(a_string + ' ', end='')
    elif top_surr:
        surround(len(a_string), '.')
    if not input_bool:
        print(a_string)
        if bot_surr:
            surround(len(a_string), u'\u02D9')


def validate_input(a_prompt='', a_type=str, surr_bool=False):

    while True:
        try:
            if not surr_bool:
                an_input = input(a_prompt)
            elif surr_bool:
                print_surround(a_prompt, True)
                an_input = input('')
            return a_type(an_input)

        except ValueError:
            pass


def validate_char_response(response_list, validation_list):
    for each_response in response_list:
        if each_response not in validation_list:
            return False
    return True


def validate_response(a_response, validation_list):
    if a_response not in validation_list:
        return False
    return True
