def parse_cl_args():

    import argparse

    parser = argparse.ArgumentParser(description='A Pootie App - play an online game of Pootie against friends and enemies alike!')

    parser.add_argument("-m", "--mode", default='cli',
                        choices=['cli', 'gui', 'controller'],
                        help="specify run mode, default is \'cli\'")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="debug mode, default is off")

    return parser.parse_args()
