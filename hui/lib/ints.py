def cli(a_logger, title, version):

    from i_o import print_surround, validate_input, validate_char_response
    from misc import call_procedure

    OPTIONS = ['(C)ontroller', '(E)xit']

    WEL_MSG = title + ' ' + version
    BYE_MSG = 'Bye!'
    OPTS = [op[op.find('(')+1] for op in OPTIONS]

    print_surround(WEL_MSG)

    while True:

        user_response = set(validate_input
                            ('OPTIONS ' + ' '.join(OPTIONS) + ' ', str))
        user_response = [re.upper() for re in user_response]

        if validate_char_response(user_response, OPTS):

            if 'C' in user_response:

                from rc import port_number
                from controller import controller

                call_procedure(a_logger, 'controller', controller,
                               [a_logger, port_number], True, True)

            if 'E' in user_response:

                break

    print_surround(BYE_MSG)


def gui(a_logger, title, version):

    from pylibui.core import App
    from pylibui.controls import Window

    class MyWindow(Window):

        def onClose(self, data):
            super().onClose(data)
            app.stop()

    app = App()

    window = MyWindow('{} {}'.format(title, version), 400, 300)
    window.setMargined(True)
    window.show()

    app.start()
    app.close()
