#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "elevate privileges to erase the system files"
   exit 1
fi

name=hui
path_prefix=/usr/local

rm $path_prefix/bin/$name

#rm -r /var/local/$name

rm -r $path_prefix/{etc,lib}/$name

rm /usr/local/lib/systemd/system/$name.service

unset name path_prefix

