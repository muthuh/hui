#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "elevate privileges to write the system files"
   exit 1
fi

name=hui
path_prefix=/usr/local

cp ../$name.py $path_prefix/bin/$name

mkdir -p /var/local/$name

mkdir -p $path_prefix/{etc,lib}/$name
cp ../dev/controller.py $path_prefix/lib/$name
cp ../lib/*.py $path_prefix/lib/$name
mv $path_prefix/lib/$name/rc.py $path_prefix/etc/$name

mkdir -p /usr/local/lib/systemd/system
cp ../util/$name.service /usr/local/lib/systemd/system/

unset name path_prefix

