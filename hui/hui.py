#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 08:40:10 2018

@author: sebastian
"""
from sys import path
from pathlib import Path
NAME = Path(__file__).stem
path_prefix = '/usr/local'
path.append('{}/lib/{}'.format(path_prefix, NAME))
path.append('{}/etc/{}'.format(path_prefix, NAME))
from datetime import datetime
from args import parse_cl_args
from log import define_logger
from misc import call_procedure
from rc import data_prefix

VER = '0.0.1 BETA TESTING'


def main(main_logger, a_cl_args):

    if cl_args.mode == 'cli':

        from ints import cli

        call_procedure(main_logger, 'cli', cli, [main_logger, NAME, VER])

    if cl_args.mode == 'gui':

        from ints import gui

        call_procedure(main_logger, 'gui', gui, [main_logger, NAME, VER], True)

    if cl_args.mode == 'controller':

        from rc import port_number
        from controller import controller

        call_procedure(main_logger, 'controller', controller,
                       [main_logger, port_number], True, True)


if __name__ == "__main__":

    cl_args = parse_cl_args()
    Path('{}/logs'.format(data_prefix)).mkdir(parents=True, exist_ok=True)
    main_logger = define_logger('/var/local/{}/logs/{}'.format
                                (NAME, datetime.now().strftime
                                 ('%Y-%m-%d')), cl_args.debug)

    try:

        main_logger.debug('~~ START ~~')

        main(main_logger, cl_args)

    except KeyboardInterrupt:

        main_logger.debug('keyboard interrupt')
        print('')

    finally:

        main_logger.debug('~~ EXIT ~~')
