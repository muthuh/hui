#!/usr/bin/env bash

script_name=${0/*\//} && script_name=${script_name/.*/}
DIR_LIB='/usr/local/'$script_name'/include'
DIR_DATA='./data/'
NODES=(`ls $DIR_DATA/reports/`)

for srce in ./include/*; do . $srce; done
#for srce in $DIR_LIB/*; do . $srce; done

finish () {
    echo-separate "\u203e"
}

trap finish EXIT

while true; do

    echo -en "\ec"
    echo-separate '_'
    read-cr "\033[1m:: ${script_name^^}\033[0m\n 1) Control\n 2) Graphs\n 3) Logs\n 4) Service\n 5) Import\n 0) EXIT"
    case $input in
    0) exit 0;;
    1) menu_control;;
    2) menu_graphs;;
    3) echo-separate "_"; echo " Not implemented"; echo-separate "\u203e"; read -n 1;;
	4) echo-separate "_"; echo " Not implemented"; echo-separate "\u203e"; read -n 1;;
	5) menu_import;;
    *) :;; esac

done

return 0 2>/dev/null
exit 0

